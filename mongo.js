/*
	Activity:

	>> Using Robo3T, repopulate our users collections using CRUD operations (Same database: session28)

//Create
	>> Add at least five of the members of your favorite fictional group or musical band into our users collection with the following fields:

		-firstName -string
		-lastName -string
		-email -string
		-password -string
		-isAdmin -boolean 
			note: initially none of the users are admin.

	>> Add 3 new courses in a new courses collection with the following fields:

		-name - string
		-price - number
		-isActive - boolean 
			note: initially all courses should be inactive

	>> Note: If the collection you are trying to add a document to does not exist yet, mongodb will add it for you. 

//Read

	>> Find all regular/non-admin users.

//Update

	>> Update the first user in the users collection as an admin.
	>> Update one of the courses as active.

//Delete

	>> Delete all inactive courses


//Add all of your query/commands here in mongo.js

*/

//  >> Users to be inserted in database
// CREATE

// COMMANDS USED TO CREATE ON ROBO3T

// To check if working 

db.users.insertOne({
	
	firstName : "Niklaus",
	lastName : "Michaelson",
	email : "nik.michaelson@originals.mov",
	password : "IamAHybrid",
	isAdmin  : false,
	
	
	

})




db.users.insertMany([
	{
		firstName : "Elijah",
		lastName : "Michaelson",
		email : "elijah.michaelson@originals.mov",
		password : "IamTheEldestSon",
		isAdmin  : false,
		
		
		
		
	},
	{
		firstName : "Rebekah",
		lastName : "Michaelson",
		email : "bekah.michaelson@originals.mov",
		password : "IamOriginal",
		isAdmin  : false,
		
		
		
	},
	{
		firstName : "Freya",
		lastName : "Michaelson",
		email : "freya.michaelson@originals.mov",
		password : "IamAWitchMichaelson",
		isAdmin  : false,
		
		
		
	},
	{
		firstName : "Marcel",
		lastName : "Gerard",
		email : "gerard.marcel@originals.mov",
		password : "IamAHybridbutIcan'tKillNiklaus",
		isAdmin  : false,
		
		
		
	},
	
])



db.users.insertMany([{
	
		course_name : "English I",
		price : 250,
		isActive : false
	
},
{
	course_name : "Calculus I",
		price : 250,
		isActive : false
},
{
	course_name : "Psychology I",
		price : 250,
		isActive : false
}])


// READ !!

db.users.find({isAdmin : false})

// UPDATE!!

// >> Updating first user as ADMIN

db.users.updateOne({isAdmin : false},{$set : {isAdmin : true}})


//   >> updating 1 course to be active 
db.users.updateOne({course_name : "English I"}, {$set : {isActive : true}})


// DELETE !!
 db.users.deleteMany({course_name : "Psychology I" })
 db.users.deleteMany({course_name : "Calculus I" })
